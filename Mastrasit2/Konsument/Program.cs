﻿using Contract;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konsument
{
    class ReceiveObserver : IReceiveObserver
    {
        public Task ConsumeFault<T>(ConsumeContext<T> context, TimeSpan duration, string consumerType, Exception exception) where T : class
        {
            Console.WriteLine("Wiadomosc zostala odebrana przez warstwe transportowa");
            return Task.CompletedTask;
        }

        public Task PostConsume<T>(ConsumeContext<T> context, TimeSpan duration, string consumerType) where T : class
        {
            Console.WriteLine("Wiadomosc zostala odebrana i obsluzona");
            return Task.CompletedTask;
        }

        public Task PostReceive(ReceiveContext context)
        {
            Console.WriteLine("Handler obsluzyl wiadomosc");
            return Task.CompletedTask;
        }

        public Task PreReceive(ReceiveContext context)
        {
            Console.WriteLine("Obsluga wiadomosci rzucila wyjatek");
            return Task.CompletedTask;
        }

        public Task ReceiveFault(ReceiveContext context, Exception exception)
        {
            Console.WriteLine("Wyjatek we wczesnej fazie");
            return Task.CompletedTask;
        }
    }

    class Handler: IConsumer<IKomunikat>
    {
        public Task Consume(ConsumeContext<IKomunikat> msg)
        {
            throw new Exception("Jakis Wyjatek");
        }
    }

    class Program
    {
        public static Task HndlFault(ConsumeContext<Fault<IKomunikat>> ctx)
        {
            foreach (var e in ctx.Message.Exceptions)
            {
                Console.WriteLine("Wyjatek: " + e.Message);
            }
            Console.WriteLine("Wiadomosc: " + ctx.Message.Message.tekst);
            return Task.CompletedTask;
        }
        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc => {
                var host = sbc.Host(new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => { h.Username("xtgdkfmw"); h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf"); });
                sbc.ReceiveEndpoint(host, "reecvqueue", ep => {
                    ep.Consumer<Handler>();
                    ep.Handler<Fault<IKomunikat>>(HndlFault);
                });
            });
            bus.ConnectReceiveObserver(new ReceiveObserver());

            bus.Start();
            Console.WriteLine("odbiorca wystartował");
            Console.ReadKey();
        }
    }
}
