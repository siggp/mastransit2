﻿using Contract;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KosumentNowy
{
    class Handler: IConsumer<INowaWiadomosc>
    {
        public Task Consume(ConsumeContext<INowaWiadomosc> msg)
        {
            Console.WriteLine(msg.Message.text + "\n" + msg.Message.nowedane);
            return Task.CompletedTask;
        }
    }

    class Program
    {

        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc => {
                var host = sbc.Host(new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => { h.Username("xtgdkfmw"); h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf"); });
                sbc.ReceiveEndpoint(host, "recvqueue", ep => {
                    ep.Consumer<Handler>();
                });
            });
            bus.Start();
            Console.WriteLine("odbiorca wystartował");
            Console.ReadKey();
        }
    }
}
