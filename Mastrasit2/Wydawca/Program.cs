﻿using Contract;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wydawca
{
    class Wiadomosc: IKomunikat
    {
        public string tekst { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc => {
                var host = sbc.Host(new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => { h.Username("xtgdkfmw"); h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf"); });
            });
            bus.Start();

            var msg = new Wiadomosc()
            {
                tekst = "Tekst wiadomosci!",
            };

            bus.Publish(msg);
            Console.ReadKey();
        }
    }
}
