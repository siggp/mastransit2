﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    public class Komunikat
    {
        public string tekst { get; set; }
    }
    interface IMesg1 { string text { get; set; } }
    interface IMesg2 { string text { get; set; } }

    public interface IStaraWiadomosc { string text { get; set; } }
    public interface INowaWiadomosc: IStaraWiadomosc { string nowedane { get; set; } }

    public interface IKomunikat
    {
        string tekst { get; set; }
    }
}
